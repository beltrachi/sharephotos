
<!-- Tables
================================================== -->
<section id="files">
  <div class="page-header">
    <h1>Fitxers</h1>
  </div>
  
  <table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th>#</th>
        <th>Nom del fitxer</th>
        <th>Fitxer</th>
        <th>Data</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($files as $file){ ?>
      <tr>
        <td><? echo $file["id"]; ?></td>
        <td><? echo $file["name"]; ?></td>
        <td><a href="<? echo $file["url"]; ?>"><? echo $file["url"]; ?></a></td>
        <td><? echo $file["date"]; ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</section>

